# OpenMAINT Installation Script



## Getting started

To make it easy for you to get started with OpenMAINT, here's a script to setup everything for you.

Simply execute the script `setup.sh` from within this folder and a new tomcat service instance with OpenMAINT installation will be created.

Afterwards navigate with a browser to http://localhost:8080/openmaint and complete the installation by inserting the PostgreSQL credentials.

## Documentation

- [Technical Manual by Tecnoteca srl](https://www.cmdbuild.org/file/manuali/technical-manual-in-english)
- [PostgreSQL packages for Debian](https://wiki.postgresql.org/wiki/Apt)
- [Adduser by Debian](https://manpages.debian.org/bullseye/adduser/adduser.8.en.html)
- [Tomcat 9 Instance by Debian](https://manpages.debian.org/bullseye/tomcat9-user/tomcat9-instance-create.2.en.html)
