#!/bin/bash
# Prerequisite: Debian like Linux OS.
#               Standard user with sudo rights.
readonly REQUIRED_PKGS="gnupg"
readonly SETUP_PKGS="\
  openjdk-11-jre-headless\
  postgresql-10 postgresql-contrib-10 postgresql-10-postgis-3 postgis libpostgis-java\
  tomcat9-user
"
readonly POSTGRESQL_JDBC="postgresql-42.3.1.jar"
readonly OPEN_MAINT_REL="2.2/openmaint-2.2-3.3.2-w.war"
readonly APT_OPT=

# Update package databases and upgrade system.
_packageUpdate() {
  sudo apt-get update
  sudo apt-get $APT_OPT upgrade
}

# Query if a package is already installed.
_packageQuery() {
  local packageName=$1
  dpkg-query -l $packageName | grep "^ii" > /dev/null
  return $?
}

# Install desired package.
_packageInstall() {
  local packageName=$1
  sudo apt-get $APT_OPT install $packageName
  return $?
}

# Check and install list of software packages, or else terminate script on error.
_packageSetup() {
  local requiredPackages="$@"
  local installPkgs=""
  for packageName in $requiredPackages; do
    _packageQuery $packageName
    if [ $? != 0 ]; then
      local installPkgs="$installPkgs $packageName"
    fi
  done
  local updatePkgs=none
  for packageName in $installPkgs; do
    if [ $updatePkgs = none ]; then
      _packageUpdate
      updatePkgs=done
    fi
    _packageInstall $packageName
    if [ $? != 0 ]; then
      exit 1
    fi
  done
}

# Add PostgreSQL repository. Needs gpg installed.
_pgRepo() {
  if [ ! -f /etc/apt/sources.list.d/pgdg.list ]; then
    # Create the file repository configuration.
    sudo sh -c 'echo "deb [signed-by=/usr/share/keyrings/pgdg-archive-keyring.gpg] http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
  fi
  if [ ! -f /usr/share/keyrings/pgdg-archive-keyring.gpg ]; then
    # Import the repository signing key.
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | gpg --dearmor | sudo tee /usr/share/keyrings/pgdg-archive-keyring.gpg >/dev/null
  fi
}

# Change postgresql default password for security reasons.
_pgPassword() {
  local newPgPw=no
  echo
  echo "Important: For security reasons it is recommended to change the password of the default"
  echo "           postgresql user."
  if [ ! -f pg.ini ]; then
    echo "           Please enter a new password here below."
    local newPgPw=yes
  else
    echo "           Password already changed. Skipping this step."
    echo
  fi
  if [ $newPgPw = yes ]; then
    echo ""
    read -rsp "Enter new postgres user password: " _pgPasswd
    echo
    sudo -u postgres psql -d template1 -c "ALTER USER postgres WITH PASSWORD '$_pgPasswd';"
    echo "Password change done. Thank you."
    echo
    touch pg.ini
  fi
}

# Creates a limited system user instance of tomcat for openmaint installation.
# Installs OpenMAINT with given release.
# See also constant OPEN_MAINT_REL at the top of the file.
_tomcatOpenMaintInstance() {
  local tomcatUsr=tomcat
  local tomcatDir=/home/$tomcatUsr
  if [ ! -d $tomcatDir ]; then
    sudo adduser --system $tomcatUsr
  fi
  if [ ! -d $tomcatDir/openmaint ]; then
    sudo su - $tomcatUsr -s /bin/bash -c "tomcat9-instance-create openmaint"
  fi
  if [ ! -d $tomcatDir/openmaint/lib ]; then
    sudo su - $tomcatUsr -s /bin/bash -c "mkdir -v openmaint/lib"
    sudo su - $tomcatUsr -s /bin/bash -c "wget https://jdbc.postgresql.org/download/$POSTGRESQL_JDBC -P openmaint/lib"
  fi
  local omBaseUrl="https://sourceforge.net/projects/openmaint/files"
  local omDownloadUrl="$omBaseUrl/$OPEN_MAINT_REL"
  if [ ! -f $tomcatDir/openmaint/webapps/openmaint.war ]; then
    sudo su - $tomcatUsr -s /bin/bash -c "wget $omDownloadUrl -O openmaint/webapps/openmaint.war"
  fi
  if [ ! -f /etc/systemd/system/openmaint.service ]; then
    sudo tee "/etc/systemd/system/openmaint.service" <<EOF
[Unit]
Description=OpenMAINT
After=network.target

[Service]
Type=forking
User=$tomcatUsr
Group=nogroup
ExecStart=$tomcatDir/openmaint/bin/startup.sh
ExecStop=$tomcatDir/openmaint/bin/shutdown.sh

[Install]
WantedBy=multi-user.target
EOF
  sudo systemctl daemon-reload
  sudo systemctl enable openmaint.service --now
  fi
  if [ `systemctl is-active openmaint` = active ]; then
    echo
    echo "OpenMAINT has been installed."
    echo "Beware that the default password for postgresql database my be changed by the previous step."
    local myIp=`hostname -I | xargs`
    echo "Navigate to http://$myIp:8080/openmaint and insert the DB credentials that you choose in"
    echo "the previous step. For example user postgres and password s3cr3tpw."
    echo
  else
    echo
    echo "Oops there went something terribly wrong."
    echo "Please check the logs."
    echo
  fi
}

_packageSetup $REQUIRED_PKGS
_pgRepo
_packageSetup $SETUP_PKGS
_pgPassword
_tomcatOpenMaintInstance

exit 0
